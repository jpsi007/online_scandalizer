#!/bin/bash

module load singularity
module use /lcrc/project/jlab/local/etc/modulefiles
module load hallac_container

RUN=$1

export SINGULARITY_BINDPATH="/scratch" 
REPLAYDIR="/lcrc/project/jlab/jpsi-007/offline/Replay"
TMPDIR="/scratch/replay-$RUN"
ODIR="$REPLAYDIR/DATA/replay"


echo "================================"
echo "Running replay for COIN run $RUN"
echo "================================"

cd ${REPLAYDIR}
mkdir -p $TMPDIR/full
mkdir -p $TMPDIR/logs
hcana -q -b "scripts/replay_production_coin.cxx+($RUN,-1)" || exit $?
chmod -R g+w $TMPDIR
rsync -va $TMPDIR/full/* $ODIR/full
mkdir -p $ODIR/log/log-$RUN
rsync -va $TMPDIR/logs/* $ODIR/log/log-$RUN
rm -rf $TMPDIR

echo "================================="
echo "Finished processing COIN run $RUN"
echo "================================="
