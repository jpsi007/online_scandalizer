#!/bin/bash

module load singularity
module use /lcrc/project/jlab/local/etc/modulefiles
module load hallac_container

#export SINGULARITY_BINDPATH="/lcrc,/scratch" 
export SINGULARITY_BINDPATH="/scratch" 

PHASE=`echo $1 | awk '{print($1);}'`
RUN=`echo $1 | awk '{print($2);}'`

TMPDIR=/lcrc/globalscratch/bduran
#SKIMDIR="/lcrc/project/jlab/data/hallc/jpsi-007/replay/skim/${PHASE}"
SKIMDIR=/lcrc/globalscratch/bduran/skim-staging/${PHASE}
REPLAYDIR="/lcrc/project/jlab/jpsi-007/offline/Replay"


echo "================================"
echo "Running skim for COIN run $RUN"
echo "================================"

mkdir -p $SKIMDIR
mkdir -p $TMPDIR

[[ -d $SKIMDIR ]] || mkdir -p $SKIMDIR

cd $REPLAYDIR
root -b -q "scripts/skim.cxx+($RUN)" || exit $?
chmod g+w $TMPDIR/skim_${RUN}.root
mv $TMPDIR/skim_${RUN}.root $SKIMDIR || exit $?

echo "================================="
echo "Finished skimming COIN run $RUN"
echo "================================="
