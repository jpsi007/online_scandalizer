#!/bin/env bash

# Load the default version of GNU parallel.
module purge
module load gcc/7.1.0-4bgguyp
module load parallel

# When running a large number of tasks simultaneously, it may be
# necessary to increase the user process limit.
ulimit -u 10000

# This specifies the options used to run srun. 
#srun="srun --exclusive -N1 -n1 -c12"
srun="bash"
JOBLOG="/home/sly2j/out/jpsi-skim.progress"

# This specifies the options used to run GNU parallel:
#
#   --delay of 0.2 prevents overloading the controlling node.
#
#   -j is the number of tasks run simultaneously.
#
#   The combination of --joblog and --resume create a task log that
#   can be used to monitor progress.
#
parallel="parallel -a ../db2/run_info.txt --delay 0.2 -j 2 --joblog ${JOBLOG} --resume --resume-failed"

# Run the script, coin_replay.sh, using GNU parallel and srun. Parallel
# will run the runtask script for the numbers 1 through $SLURM_NTASKS 
$parallel "$srun ./skim.sh {1}"
