#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>
#include "fmt/format.h"
#include "nlohmann/json.hpp"

#include "ROOT/RDataFrame.hxx"
#include "ROOT/RVec.hxx"

#include "Math/Vector3D.h"
#include "Math/Vector4D.h"
#include "Math/VectorUtil.h"

#include "TSystem.h"

// Important this script runs single-threaded, as we make some unsafe asumptions about
// this. Not a big deal anyway as this script is very fast
constexpr const unsigned kNumThreads = 1;

// Create a set of dataframe definitions for this node based on the values
// in the map vars(name, var) where each new var with the name "name"
// will have as value var[i] - var[i-1]
ROOT::RDF::RNode define_deltas(ROOT::RDF::RNode                         d,
                               const std::map<std::string, std::string> vars) {
  using scaler_map = std::map<std::string, decltype(d.Take<double>(""))>;

  scaler_map scalers;
  for (const auto& [name, var] : vars) {
    scalers[name] = d.Take<double>(var);
  }
  // create a utility index, and then define our new variables
  d = d.Define("index",
               []() {
                 static unsigned i = 0;
                 return i++;
               },
               {});
  for (auto& [name, list] : scalers) {
    std::vector<double> tmp = *list;

    d = d.Define(name,
                 [=](unsigned idx) {
                   const double prev = (idx > 0) ? tmp[idx - 1] : 0;
                   const double cur  = tmp[idx];
                   return cur - prev;
                 },
                 {"index"});
  }
  return d;
}
// Process many defines, similar to define_deltas
ROOT::RDF::RNode define_many(ROOT::RDF::RNode                         d,
                             const std::map<std::string, std::string> vars) {
  for (const auto& [name, var] : vars) {
    d.Define(name, var);
  }
  return d;
}

void scaler_skim(int run = 7146) {
  // ===============================================================================================
  // Locate the input file
  // ===============================================================================================
  const int   nevents  = -1;
  std::string rootfile = fmt::format("/lcrc/project/jlab/data/hallc/jpsi-007/replay/full/"
                                     "coin_replay_production_{}_{}.root",
                                     run, nevents);
  std::cout << "attempting to load file: " << rootfile << std::endl;
  // ===============================================================================================
  // Dataframe
  // ===============================================================================================

  ROOT::EnableImplicitMT(kNumThreads);

  // SHMS Scaler tree
  // SHMS
  ROOT::RDataFrame d_sp("TSP", rootfile);
  // HMS
  ROOT::RDataFrame d_sh("TSH", rootfile);

  std::map<std::string, std::string> cvars{{"BCM1_current", "P.BCM1.scalerCurrent"},
                                           {"BCM4A_current", "P.BCM4A.scalerCurrent"},
                                           {"BCM4B_current", "P.BCM4B.scalerCurrent"},
                                           {"event", "evNumber"},
                                           {"run", std::to_string(run)}};
  auto                               pvars = cvars;
  std::map<std::string, std::string> hvars{{"BCM1_current", "H.BCM1.scalerCurrent"},
                                           {"BCM4A_current", "H.BCM4A.scalerCurrent"},
                                           {"BCM4B_current", "H.BCM4B.scalerCurrent"},
                                           {"event", "evNumber"},
                                           {"run", std::to_string(run)}};

  std::map<std::string, std::string> cdeltavars = {
      {"num_events", "evNumber"},
      {"BCM1_charge", "P.BCM1.scalerCharge"},
      {"BCM4A_charge", "P.BCM4A.scalerCharge"},
      {"BCM4B_charge", "P.BCM4B.scalerCharge"},
      {"pTRIG1", "P.pTRIG1.scaler"},
      {"pTRIG2", "P.pTRIG2.scaler"},
      {"pTRIG3", "P.pTRIG3.scaler"},
      {"pTRIG4", "P.pTRIG4.scaler"},
      {"pTRIG5", "P.pTRIG5.scaler"},
      {"pTRIG6", "P.pTRIG6.scaler"},
      {"time", "P.1MHz.scalerTime"},
      {"EDTM", "P.EDTM.scaler"},
      {"pEL_CLEAN", "P.pEL_CLEAN.scaler"},
      {"hEL_CLEAN", "P.hEL_CLEAN.scaler"}};

  std::map<std::string, std::string> pdeltavars = {
      {"num_events", "evNumber"},
      {"BCM1_charge", "P.BCM1.scalerCharge"},
      {"BCM4A_charge", "P.BCM4A.scalerCharge"},
      {"BCM4B_charge", "P.BCM4B.scalerCharge"},
      {"S1X", "P.S1X.scaler"},
      {"S1Y", "P.S1Y.scaler"},
      {"S2X", "P.S2X.scaler"},
      {"S2Y", "P.S2Y.scaler"},
      {"pTRIG1", "P.pTRIG1.scaler"},
      {"pTRIG2", "P.pTRIG2.scaler"},
      {"pTRIG3", "P.pTRIG3.scaler"},
      {"pTRIG4", "P.pTRIG4.scaler"},
      {"pTRIG5", "P.pTRIG5.scaler"},
      {"pTRIG6", "P.pTRIG6.scaler"},
      {"time", "P.1MHz.scalerTime"},
      {"EDTM", "P.EDTM.scaler"},
      {"pEL_CLEAN", "P.pEL_CLEAN.scaler"}};
  std::map<std::string, std::string> hdeltavars = {
      {"num_events", "evNumber"},
      {"BCM1_charge", "H.BCM1.scalerCharge"},
      {"BCM4A_charge", "H.BCM4A.scalerCharge"},
      {"BCM4B_charge", "H.BCM4B.scalerCharge"},
      {"S1X", "H.S1X.scaler"},
      {"S1Y", "H.S1Y.scaler"},
      {"S2X", "H.S2X.scaler"},
      {"S2Y", "H.S2Y.scaler"},
      {"hTRIG1", "H.hTRIG1.scaler"},
      {"hTRIG2", "H.hTRIG2.scaler"},
      {"hTRIG3", "H.hTRIG3.scaler"},
      {"hTRIG4", "H.hTRIG4.scaler"},
      {"hTRIG5", "H.hTRIG5.scaler"},
      {"hTRIG6", "H.hTRIG6.scaler"},
      {"time", "H.1MHz.scalerTime"},
      {"EDTM", "H.EDTM.scaler"},
      {"hEL_CLEAN", "H.hEL_CLEAN.scaler"}};

  // Now define our child dataframe

  auto dc = define_deltas(define_many(d_sp, cvars), cdeltavars);
  auto ds = define_deltas(define_many(d_sp, svars), pdeltavars);
  auto dh = define_deltas(define_many(d_sh, hvars), hdeltavars);

  auto get_all_keys =
      [](const std::vector<std::map<std::string, std::string>>& map_list) {
        std::vector<std::string> ret;
        for (const auto& m : map_list) {
          for (const auto& [key, var] : map) {
            ret.push_back(key);
          }
        }
        return ret;
      };

  auto ovars_coin = get_all_keys({cvars, cdeltavars});
  auto ovars_shms = get_all_keys({pvars, pdeltavars});
  auto ovars_hms  = get_all_keys({hvars, hdeltavars});

  // ===============================================================================================
  // Skim the scaler trees
  // ===============================================================================================
  std::string ofname =
      fmt::format("/lcrc/globalscratch/{}/skim_{}.root", getenv("USER"), run);

  ROOT::RDF::RSnapshotOptions opts;
  opts.fMode = "UPDATE";
  dc.Snapshot("coin_scaler", ofname, ovars_coin, opts);
  ds.Snapshot("shms_scaler", ofname, ovars_shms, opts);
  dh.Snapshot("hms_scaler", ofname, ovars_hms, opts);
}
