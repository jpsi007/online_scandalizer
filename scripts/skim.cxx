#include <cmath>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <string>
#include <valarray>
#include "fmt/format.h"
#include "nlohmann/json.hpp"

#include "ROOT/RDataFrame.hxx"
#include "ROOT/RVec.hxx"

#include "Math/Vector3D.h"
#include "Math/Vector4D.h"
#include "Math/VectorUtil.h"

#include "TSystem.h"

constexpr const unsigned kNumThreads = 12;

// =================================================================================
// Constants
// =================================================================================
constexpr const double M_P  = .938272;
constexpr const double M_P2 = M_P * M_P;
constexpr const double M_J  = 3.096916;
constexpr const double M_J2 = M_J * M_J;
constexpr const double M_e  = .000511;
constexpr const double M_mu = .1056583745;
// =================================================================================
// Cuts -- (Significantly) looser than the standard analysis cuts
// =================================================================================
std::string shms_good_track = "P.gtr.dp > -15 && P.gtr.dp < 25";
std::string hms_good_track  = "H.gtr.dp > -11 && H.gtr.dp < 11";
std::string shms_good_pid   = "P.cal.etottracknorm > 0.0 && P.cal.etottracknorm < 2.";
std::string hms_good_pid    = "H.cal.etottracknorm > 0.0 && H.cal.etottracknorm < 2.";

// =================================================================================
// Definitions
// =================================================================================
using Pvec3D = ROOT::Math::XYZVector;
using Pvec4D = ROOT::Math::PxPyPzMVector;

// =================================================================================
// J/psi reconstruction --> apply central momentum corrections from Mark
// =================================================================================
auto p_track = [](double px, double py, double pz) { return Pvec4D{px, py, pz, M_e}; };
auto p_jpsi  = [](const Pvec4D& e1, const Pvec4D& e2) { return e1 + e2; };
auto E_gamma = [](const Pvec4D& jpsi) {
  double res = (jpsi.M2() - 2. * jpsi.E() * M_P) /
               (2. * (jpsi.E() - M_P - jpsi.P() * cos(jpsi.Theta())));
  return res;
};
auto abst = [](const double Egamma, Pvec4D& jpsi) {
  Pvec4D beam{0, 0, Egamma, 0};
  return fabs((beam - jpsi).M2());
};

// each entry
std::vector<double> delta_vector(std::vector<double> in_vec) {
  const unsigned        size = in_vec.size();
  std::valarray<double> vmax{in_vec.data(), size};
  in_vec.insert(in_vec.begin(), 0);
  std::valarray<double> vmin{in_vec.data(), size};
  std::valarray<double> res = vmax - vmin;
  return {std::begin(res), std::end(res)};
}
// in kHz
std::vector<double> rate_vector(const std::vector<double>& scaler,
                                const std::vector<double>& time) {
  auto ds = delta_vector(scaler);
  // time in seconds
  auto dt = delta_vector(time);

  // seconds to kHz
  std::valarray<double> res = std::valarray<double>(ds.data(), ds.size()) /
                              (1000 * std::valarray<double>(dt.data(), dt.size()));
  return {std::begin(res), std::end(res)};
}

void skim(int run = 7146, int nevents = -1) {
  std::string rootfile = fmt::format("/lcrc/project/jlab/data/hallc/jpsi-007/replay/full/"
                                     "coin_replay_production_{}_{}.root",
                                     run, nevents);
  // ===============================================================================================
  // Dataframe
  // ===============================================================================================

  ROOT::EnableImplicitMT(kNumThreads);

  //---------------------------------------------------------------------------
  // Detector tree
  ROOT::RDataFrame d("T", rootfile);

  // SHMS Scaler tree
  ROOT::RDataFrame d_sp("TSP", rootfile);

  // Extract beam current information from the scaler tree
  std::cout << "Extracting beam current information from the scaler tree"
            << "\n";
  auto BCM1_list         = d_sp.Take<double>("P.BCM1.scalerCurrent");
  auto BCM4A_list        = d_sp.Take<double>("P.BCM4A.scalerCurrent");
  auto BCM4B_list        = d_sp.Take<double>("P.BCM4B.scalerCurrent");
  auto pTRIG1_list       = d_sp.Take<double>("P.pTRIG1.scaler");
  auto pTRIG4_list       = d_sp.Take<double>("P.pTRIG4.scaler");
  auto pTRIG6_list       = d_sp.Take<double>("P.pTRIG6.scaler");
  auto time_list         = d_sp.Take<double>("P.1MHz.scalerTime");
  auto scaler_event_list = d_sp.Take<double>("evNumber");

  auto shms_rate = rate_vector(*pTRIG1_list, *time_list);
  auto hms_rate  = rate_vector(*pTRIG4_list, *time_list);
  auto coin_rate = rate_vector(*pTRIG6_list, *time_list);

  // Index to keep track of all the positions in our kNumThreads threads in parallel
  std::array<unsigned, kNumThreads> scaler_idx = {0};
  // Function to lookup the relevant value from the scaler tree
  auto lookup_scaler_position = [&](unsigned slot, unsigned int event) {
    unsigned idx = scaler_idx[slot];
    // lookup correct position in the array if we are not at the end yet
    while (idx < (scaler_event_list->size() - 1) && event > scaler_event_list->at(idx)) {
      ++idx;
    }
    scaler_idx[slot] = idx;
    return idx;
  };

  auto d_coin = d.Filter("fEvtHdr.fEvtType == 4");

  // Good track cuts
  auto d_tracked =
      d_coin.Filter(fmt::format("{} && {}", hms_good_track, shms_good_track))
          .Define("p_shms", p_track, {"P.gtr.px", "P.gtr.py", "P.gtr.pz"})
          .Define("p_hms", p_track, {"H.gtr.px", "H.gtr.py", "H.gtr.pz"})
          .Define("p_jpsi", p_jpsi, {"p_shms", "p_hms"})
          .Define("M_jpsi", "p_jpsi.M()")
          .Define("E_gamma", E_gamma, {"p_jpsi"})
          .Define("abst", abst, {"E_gamma", "p_jpsi"})
          .Define("coin_time", "CTime.ePositronCoinTime_ROC2")
          .Define("run", std::to_string(run))
          .Define("event", "fEvtHdr.fEvtNum")
          .DefineSlot("scaler_index",
                      [&](unsigned slot, unsigned event) {
                        unsigned idx = lookup_scaler_position(slot, event);
                        return idx;
                      },
                      {"event"})
          .Define("BCM1_current",
                  [&](unsigned idx) { return double(BCM1_list->at(idx)); },
                  {"scaler_index"})
          .Define("BCM4A_current",
                  [&](unsigned idx) { return double(BCM4A_list->at(idx)); },
                  {"scaler_index"})
          .Define("BCM4B_current",
                  [&](unsigned idx) { return double(BCM4B_list->at(idx)); },
                  {"scaler_index"})
          .Define("shms_rate", [&](unsigned idx) { return shms_rate[idx]; },
                  {"scaler_index"})
          .Define("hms_rate", [&](unsigned idx) { return hms_rate[idx]; },
                  {"scaler_index"})
          .Define("coin_rate", [&](unsigned idx) { return coin_rate[idx]; },
                  {"scaler_index"})
          .Define("P_ngcer_npe", [](ROOT::RVec<double> array) { return array; },
                  {"P.ngcer.npe"})
          .Filter(
              [](double shms_rate, double hms_rate) {
                return !std::isnan(shms_rate) && !std::isnan(hms_rate);
              },
              {"shms_rate", "hms_rate"});

  // PID cuts
  std::string skim_cut = fmt::format("({} && {})", shms_good_pid, hms_good_pid);
  auto        d_skim   = d_tracked.Filter(skim_cut);

  // ===============================================================================================
  // Skim the event tree
  // ===============================================================================================
  std::string ofname =
      fmt::format("/lcrc/globalscratch/{}/skim_{}.root", getenv("USER"), run);

  d_skim.Snapshot("Tjpsi", ofname,
                  {"run",
                   "event",
                   "scaler_index",
                   "BCM1_current",
                   "BCM4A_current",
                   "BCM4B_current",
                   "shms_rate",
                   "hms_rate",
                   "coin_rate",
                   "P.react.x",
                   "P.react.y",
                   "P.react.z",
                   "P.gtr.dp",
                   "P.gtr.th",
                   "P.gtr.ph",
                   "P.gtr.y",
                   "P.gtr.x",
                   "P.dc.x_fp",
                   "P.dc.xp_fp",
                   "P.dc.y_fp",
                   "P.dc.yp_fp",
                   "P.cal.eprtrack",
                   "P.cal.eprtracknorm",
                   "P.cal.etot",
                   "P.cal.etotnorm",
                   "P.cal.etottracknorm",
                   "P.cal.etrack",
                   "P.cal.etracknorm",
                   "P.ngcer.npeSum",
                   "P_ngcer_npe",
                   "P.ngcer.xAtCer",
                   "P.ngcer.yAtCer",
                   "P.hod.goodscinhit",
                   "P.hod.betanotrack",
                   "P.dc.ntrack",
                   "H.gtr.dp",
                   "H.gtr.th",
                   "H.gtr.ph",
                   "H.gtr.y",
                   "H.gtr.x",
                   "H.react.x",
                   "H.react.y",
                   "H.react.z",
                   "H.cal.eprtrack",
                   "H.cal.eprtracknorm",
                   "H.cal.etot",
                   "H.cal.etotnorm",
                   "H.cal.etottracknorm",
                   "H.cal.etrack",
                   "H.cal.1pr.eplane",
                   "H.cal.2ta.eplane",
                   "H.cal.3ta.eplane",
                   "H.cal.4ta.eplane",
                   "H.cal.etracknorm",
                   "H.cer.npeSum",
                   "H.hod.goodscinhit",
                   "H.hod.betanotrack",
                   "H.dc.ntrack",
                   "coin_time",
                   "p_shms",
                   "p_hms",
                   "p_jpsi",
                   "M_jpsi",
                   "E_gamma",
                   "abst"});

  // ===============================================================================================
  // Write the total charge to the file
  // ===============================================================================================
  // scalers
  auto   total_charge      = d_sp.Max("P.BCM1.scalerChargeCut");
  double good_total_charge = *total_charge / 1000.0;  // mC
  TFile  ofile(ofname.c_str(), "update");
  TH1D*  ocharge = new TH1D("total_good_charge", "total_good_charge", 1, 0, 1);
  ocharge->SetBinContent(1, good_total_charge);
  ocharge->Write();
  ofile.Close();
}
