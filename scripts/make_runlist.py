#!/bin/env python3

import json
import os

oname='db2/run_info.txt'
dbname = 'db2/jpsi_status.json'

print('Reading configuration from: ', dbname)
runs = []
with open(dbname, 'r') as dbfile:
    dbstring = dbfile.readlines()
    db = json.loads(''.join(dbstring))
    for setting in db:
        new_runs = [(setting, run) for run in db[setting]['good_run_list']]
        runs.extend(new_runs)
print('Writing run info to {}'.format(oname))
with open(oname, 'w') as ofile:
    for run in runs:
        ofile.write('{} {}\n'.format(*run))
